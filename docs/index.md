# Introducción General
## ¿Qué es RTLS?
El término RTLS (del inglés *Real Time Location System*) es usado para denominar a los sistemas de localización de tiempo real. Este término engloba tanto las tecnologías pensadas para ser usadas en exteriores, como las pensadas para usarse dentro de edificios. Habitualmente con RTLS se refiere a sistemas de posicionamiento en interiores (en inglés, IPS *indoor location system*).

### Características comunes a los sistemas de interior y exterior
La capa física de estos sitemas es de RF (radiofrequencia)[@wiki_rtls]. Tanto los sistemas para interiores como los pensados para exteriores, cuentan con puntos fijos de referencia con ubicación conocida y objetos en movimiento cuya ubicación deseamos conocer. Los puntos de referencia y los de ubicación desconocida se comunican para poder hacer un cálculo de la posición.

#### Trilateración
Aunque algunos sistemas, como los basados en más de una antena por dispositivo, usan triangulación (cálculo de ángulos), la mayoría usa trilateración, es decir, cálculan la posición basados en la distancia a los puntos de referencia.

![](img/Trilateracion-La-distancia-a-tres-balizas-nodos-blancos-permite-a-un-sensor-nodo_W640.jpg)  
*Figura 1: Calculo de la ubicación conociendo la distancia a los puntos de referencia[@trilateration]*


#### Cálculo badado en la intensidad de señal
Una de las herramientas para calcular la distancia es la intensidad de la señal recibida, un indicador común de esta magnitud es el indicador de fuerza de la señal recibida (RSSI por las siglas del inglés *Received Signal Strength Indicator*). El RSSI está disponible en varios protocolos de comunicación inalámbrica como WiFi y Bluetooth.

El problema de esta magnitud es que se ve altamente afectada por los obstáculos entre transmisor y receptor. En el caso de los sistemas de localización en interiores, abundan los obstáculos y se dificulta el uso del RSSI. Además, está se ve afectada por las fluctuaciones en la potencia de transmición.

Entonces, por un lado es fácil acceder a este indicador y por otro lado los obstáculos y variaciones de potencia, dificultan su utilización. Como una manera de mitigar el problema de los obstáculos, algunos sistemas implementan complejos sistemas que generan una base de datos de valores de RSSI para puntos de referencia conocidos[@fingerprint], luego cuando un objeto obtiene un valor de RSSI se apoya en dicha base de datos para conecer la distancia en función de la forma en la que la señal se comporta en ese escenario específico.

Para contrarrestar las fluctuaciones de potencia se implemtentan filtros basados en el monitoreo de dichas variaciones para poder ser compensadas a la hora de calcular la posición desconocida[@rssi_correction]

#### Cálculo basado en el tiempo de tránsito
Otra herramienta para calcular la distancia entre transmisor y receptor, es el tiempo que tarda una señal en propagarse entre ambos. Existen varios indicadores, y los más efectivos son los que involucran tiempo de transmición y respuesta. Es decir, tanto los dispositivos de referencia como los de ubicación desconocida tienen que ser capaces de enviar y recibir.

En el supuesto de que sólo se trate de transmisores de referencia y receptores que buscan conocer su localización, debería existir una sincronización extremadamente exacta entre los relojes de ambos, pues los tiempos que se miden están en el orden de los nanosegundos y debido a la velocidad de propagación de señales de radio, una diferencia de algunos nanosegundos representa varios centímetros. Este tema se cubrirá en detalle cuando se trabaje sobre la implementación del sistema.

Sobre los indicadores más utilizados para medir distancia están:
- [@giovanelli]

### Diferencia entre localización en interiores y exteriores
Tal como se mencionó arriba, ambos sistemas comparten la existencia de puntos de ubicación conocida y objetos de ubicación desconocida. Tanto en interiores como en exteriores, para poder calcular su propia ubicación, los puntos de ubicación desconocida se comunican con los puntos de referencia.

La diferencia es que los sistemas más populares de ubicación en exteriores trabajan sobre tecnología satelital y son demonimados sistemas de geoposicionamiento porque nos sirven para ubicarnos sobre la superficie de la tierra. En exterior, a cielo abierto, con muy baja potencia se puede recibir la señal de un satélite por tener línea de vista entre transmisor y receptor. En interiores se hace imposible acceder a estos satélites de refencia porque los techos actúan como un gran obstáculo para la recepción de la señal.

Es por eso que en interiores, los sistemas cuentan con transmisores RF de ubicación conocida dentro del local donde se quiere conocer la ubicación.

Básicamente, la diferencia yace en que GPS, GLONASS, y otros sistemas satelitales, funcionan mientras haya línea de vista con los satélites. Los sistemas de interior necesitan que se instalen transmisores de RF en puntos de referencia para que los receptores puedan calcular su ubicación.

A su vez, no es común que los sistemas de localización de tiempo real incluyan velocidad, dirección u orientación espacial[@wiki_rtls].

En RTLS cobra importancia la exactitud y la frecuencia con la que se actualiza la posición del objeto que se quiere ubicar. Esto es muy diferente a los sistemas como GPS que tienen una exactitud del orden de algunos metros (menos de 8) en el eje horizontal y en el eje vertical[@gps_accuracy]

\bibliography
