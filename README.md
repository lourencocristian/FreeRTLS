[[_TOC_]]
# FreeRTLS
## Objetivo del proyecto
- Brindar un proyecto base para implementar un sistema de posicionamiento en interiores utilizando UWB

## Documentación
Por favor visite https://freertls.gitlab.io/FreeRTLS para conocer cómo utilizar *FreeRTLS*. Allí encontrará:
- ¿Cómo descargar la versión de código fuente que me interesa?

## Estructura del proyecto
### Web de documentación
En la carpeta [docs](docs) se encuentran los archivos que forman la web de documentación de *FreeRTLS*.

### Módulos
En la carpeta [modules](modules) se encuentran las referencias a los proyectos que constituyen *FreeRTLS*. Los proyectos están agregados como *git submodules* y tienen referencia al *commit* correspondiente a la versión de *FreeRTLS* que se está viendo.
